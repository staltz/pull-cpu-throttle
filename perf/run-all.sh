#!/bin/sh

mkdir -p results

echo "benchmark unlimited..."
node unlimited > results/unlimited.csv
echo "benchmark c90w144..."
node limited 90 144 > results/c90w144.csv
echo "benchmark c90w150..."
node limited 90 150 > results/c90w150.csv
echo "benchmark c90w200..."
node limited 90 200 > results/c90w200.csv
echo "benchmark c90w300..."
node limited 90 300 > results/c90w300.csv
echo "benchmark c90w120..."
node limited 90 120 > results/c90w120.csv
echo "benchmark c90w90..."
node limited 90 90 > results/c90w90.csv
echo "benchmark c90w60..."
node limited 90 60 > results/c90w60.csv
echo "benchmark c90w20..."
node limited 90 20 > results/c90w20.csv
echo "benchmark c80w60..."
node limited 80 60 > results/c80w60.csv
echo "benchmark c50w60..."
node limited 50 60 > results/c50w60.csv
echo "benchmark c20w60..."
node limited 20 60 > results/c20w60.csv
echo "done"

