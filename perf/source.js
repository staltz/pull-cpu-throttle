var pull = require('pull-stream');
var fs = require('fs');

module.exports = pull(
  pull.count(2e5),
  pull.asyncMap((x, cb) => fs.readFile('../package.json', cb)),
  pull.map(contents =>
    Buffer.from(contents.toString('hex').toUpperCase())
      .toString('base64')
      .toLowerCase()
      .substr(0, 10 + Math.round(Math.random() * 100)),
  ),
);
