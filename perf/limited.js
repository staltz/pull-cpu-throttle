var pull = require('pull-stream');
var usage = require('cpu-percentage');
var source = require('./source');
var cpuThrottle = require('./implementation');

const ceiling = +process.argv[2];
const wait = +process.argv[3];
if (process.argv.length !== 4 || isNaN(ceiling) || isNaN(wait)) {
  console.error('Requires two numerical arguments: <ceiling> <wait>');
  process.exit(1);
}

var start = usage();
process.stdout.write(`"time","cpu_percent"\n`);
process.stdout.write(`${start.time},${start.percent}\n`);
var count = 0;

pull(
  source,
  cpuThrottle({ceiling, wait}),
  pull.drain(() => {
    count += 1;
    if (count % 10000 === 0) {
      const stats = usage(start);
      process.stdout.write(`${stats.time},${stats.percent}\n`);
    }
  }),
);
