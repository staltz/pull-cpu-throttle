var Pausable = require('pull-pause');
var usage = require('cpu-percentage');

module.exports = function cpuThrottle(opts) {
  const opts2 = opts || {};
  const ceiling = opts2.ceiling || 88;
  const wait = opts2.wait || 144;
  const pausable = Pausable();
  let step = 2;
  let i = 0;
  let stats = usage();
  const start = stats;

  function checkpoint() {
    stats = usage(start);
    //#region THIS IS THE ONLY THING THAT CHANGED COMPARED TO ../index.js
    process.stdout.write(`${stats.time},${stats.percent}\n`);
    //#endregion
    if (stats.percent < ceiling || stats.time < 20) {
      step = step << 1 || 1;
      pausable.resume();
    } else {
      step = step >> 1 || 1;
      pausable.pause();
      setTimeout(checkpoint, wait);
    }
  }

  return function(outerRead) {
    const innerRead = pausable(outerRead);
    return function next(abort, cb) {
      innerRead(abort, function(end, data) {
        if (++i >= step) {
          i = 0;
          checkpoint();
        }
        cb(end, data);
      });
    };
  };
};
