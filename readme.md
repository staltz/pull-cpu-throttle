# pull-cpu-throttle

> A pull-stream "through stream" that does CPU throttling to avoid overheating the CPU

```bash
npm install --save pull-cpu-throttle
```

## Usage

### Default use

```js
const pull = require('pull-stream');
const cpuThrottle = require('pull-cpu-throttle');

pull(
  pull.count(1000000),
  cpuThrottle(),
  pull.drain(x => console.log(x)),
);
```

### Tweaking the parameters

This module pulls from the source, but once in a while it will check the CPU usage, and if it has gone above the limit known as the `ceiling`, it pauses for `wait` milliseconds, and then resumes, in such a way that it should stay close to the limit. In other words, two parameters control the pulling:

- `ceiling`: the maximum CPU usage you want the task to consume, approximately, in percentages (`100` is 100%, not `1`)
- `wait`: the waiting period, in milliseconds, to pause in order to allow other tasks to use the CPU

The default CPU ceiling is `88%` and the default waiting period for each pause is `144ms` (roughly 9 frames if you have the UI running at 60fps). This means that the longest pause during which no pull will occur is 144ms.

To configure your own parameters, pass an `opts` object as the argument:

```js
const pull = require('pull-stream');
const cpuThrottle = require('pull-cpu-throttle');

pull(
  pull.count(1000000),
  cpuThrottle({ceiling: 90, wait: 90}),
  pull.drain(x => console.log(x)),
);
```

To configure these parameters, consider that:

- The **greater the `ceiling`** is, the closer the stream will behaves as if you were had no `cpuThrottle()`, i.e. the *less gently* it pulls the source with regards to CPU load
- The **smaller the `ceiling`** is, the more time it will take to consume the source, i.e. the slower your application will run overall
- The **greater the `wait`**, the more the actual CPU usage fluctuates below and above the `ceiling`, i.e. the more bumpy the ride is for CPU usage and workload throughput
- The **smaller the `wait`**, the more the actual CPU usage accurately meets the `ceiling`, but also the more overhead there is with many short-lived timers for those pauses

The total time for drainage is also important. Having no `cpuThrottle()` is the fastest, with the shortest total time. Adding `cpuThrottle()` with a small `wait` might give a total drainage time of approx. 2.5x that of `pull.drain`. The defaults `ceiling=88`, `wait=144` are a sweet spot, and it can achieve a total drainage time of approx 1.4x that of pure `pull.drain` without CPU throttling. You can run benchmarks yourself by running `cd perf && ./run-all.sh` in this repository.

The chart below shows results for running the benchmark in `perf` on Ubuntu 18.04.3 x86_64, Intel® Core™ i7-7500U CPU @ 2.70GHz × 4, 15,4 GiB RAM, for different values of ceiling (`c`) and wait (`w`).

![chart with benchmark results](./chart.png)

## License

[MIT](https://tldrlegal.com/license/mit-license)
