var pull = require('pull-stream');
var test = require('tape');
var usage = require('cpu-percentage');
var fs = require('fs');
var cpuThrottle = require('./index');

test('it can be used without arguments', function(t) {
  t.plan(1);

  const start = usage();
  pull(
    pull.count(20000),
    pull.asyncMap((x, cb) => fs.readFile('./package.json', cb)),
    pull.map(contents =>
      Buffer.from(contents.toString('hex').toUpperCase())
        .toString('base64')
        .toLowerCase()
        .substr(0, 20),
    ),
    cpuThrottle(),
    pull.drain(
      () => {},
      () => {
        const stats = usage(start);
        console.log(usage(start));
        t.true(stats.percent < 99, 'CPU usage was lower than 100%');
        t.end();
      },
    ),
  );
});

test('heavy processing is below the prescribed CPU 90% ceiling', function(t) {
  t.plan(1);

  const ceiling = 90;

  const start = usage();
  pull(
    pull.count(20000),
    pull.asyncMap((x, cb) => fs.readFile('./package.json', cb)),
    pull.map(contents =>
      Buffer.from(contents.toString('hex').toUpperCase())
        .toString('base64')
        .toLowerCase()
        .substr(0, 20),
    ),
    cpuThrottle({ceiling, wait: 40}),
    pull.drain(
      () => {},
      () => {
        const stats = usage(start);
        console.log(usage(start));
        t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 90%');
        t.end();
      },
    ),
  );
});

test('heavy processing is below the prescribed CPU 50% ceiling', function(t) {
  t.plan(1);

  const ceiling = 50;

  const start = usage();
  pull(
    pull.count(8000),
    pull.asyncMap((x, cb) => fs.readFile('./package.json', cb)),
    pull.map(contents =>
      Buffer.from(contents.toString('hex').toUpperCase())
        .toString('base64')
        .toLowerCase()
        .substr(0, 20),
    ),
    cpuThrottle({ceiling, wait: 40}),
    pull.drain(null, () => {
      const stats = usage(start);
      console.log(usage(start));
      t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 50%');
      t.end();
    }),
  );
});
